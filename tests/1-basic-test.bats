#!/usr/bin/env bats

# Do not use random numbers.
# According to the docs, the entire file is evaluated after each test run.

# In case failure, debug by setting the following before [ $status == x ]
# Check https://github.com/bats-core/bats-core/blob/v1.2.0/test/test_helper.bash
# printf '%s\n' 'output:' "$output" >&2
# printf '%s\n' 'status:' "$status" >&2


# setup() is executed everytime before a test is called.
setup() {
  export WATCH_NAMESPACE=default
}

@test "Test creation and deletion of LanDB Sets" {
    export CR_NAME=ci-set-${CI_JOB_ID}
    export SET_NAME="CI TEST SET "${CI_JOB_ID} 
    export LABEL_SELECTOR="\"\""

    run bash -c "envsubst < tests/LandbSet.yaml | oc create -n ${WATCH_NAMESPACE} -f -"
    printf '%s\n' 'output:' "$output" >&2
    [ "$status" == 0 ]
    
    COMMAND="python3.6 roles/files/main.py --landb-user ${LANDB_USER} --landb-password ${LANDB_PASSWORD} --command set-search --landb-set-name \"${SET_NAME}\""
    EXPECTED_RESULT=${SET_NAME}
    run ensure_command_result
    printf '%s\n' 'output:' "$output" >&2
    [ "$status" == 0 ]

    run bash -c "oc delete landbset ${CR_NAME} -n ${WATCH_NAMESPACE}"
    printf '%s\n' 'output:' "$output" >&2
    [ "$status" == 0 ]

    COMMAND="python3.6 roles/files/main.py --landb-user ${LANDB_USER} --landb-password ${LANDB_PASSWORD} --command set-search --landb-set-name \"${SET_NAME}\""
    EXPECTED_RESULT="ERROR: No sets found that matched the provided name/expression"
    run ensure_command_result
    printf '%s\n' 'output:' "$output" >&2
    [ "$status" == 0 ]
}

@test "Test creation and deletion of LanDB Delegated Domains" {
    export CR_NAME=ci-delegated-domain-${CI_JOB_ID}
    export DOMAIN=ci-domain-${CI_JOB_ID}.cern.ch
    export DESCRIPTION="\"CAN BE REMOVED: This is a test CI delegated domain\""

    run bash -c "envsubst < tests/DelegatedDomain.yaml | oc create -n ${WATCH_NAMESPACE} -f -"
    printf '%s\n' 'output:' "$output" >&2
    [ "$status" == 0 ]
    
    # Due to the way set-search return is formated we will remove single quote (\047) and comma to facilitate comparision, we will also print 
    # in lower case since lanDB always returns in capital case
    COMMAND="python3.6 roles/files/main.py --landb-user ${LANDB_USER} --landb-password ${LANDB_PASSWORD} --command dns-search --domain .${DOMAIN} | tr -d '\047,' | awk '/Domain/ {print tolower(\$2)}'"
    EXPECTED_RESULT=.${DOMAIN}
    run ensure_command_result
    printf '%s\n' 'output:' "$output" >&2
    [ "$status" == 0 ]

    run bash -c "oc delete delegateddomain ${CR_NAME} -n ${WATCH_NAMESPACE}"
    printf '%s\n' 'output:' "$output" >&2
    [ "$status" == 0 ]

    COMMAND="python3.6 roles/files/main.py --landb-user ${LANDB_USER} --landb-password ${LANDB_PASSWORD} --command dns-search --domain .${DOMAIN}"
    EXPECTED_RESULT="ERROR: No delegated domains found that matched the provided name/expression"
    run ensure_command_result
    printf '%s\n' 'output:' "$output" >&2
    [ "$status" == 0 ]
}

ensure_command_result() {
  MAX_WAIT=180 # 3 minutes
  WAIT_UNTIL=$[ $SECONDS + $MAX_WAIT ]
  while [ $SECONDS -le $WAIT_UNTIL ]; do
    CURRENT_RESULT=$(eval "$COMMAND")
    if  [ "${CURRENT_RESULT}" == "${EXPECTED_RESULT}" ]; then
      exit 0
    fi
    echo "Result did not match, trying again in 30s"
    sleep 20
  done
  echo "Last run got result \"$CURRENT_RESULT\" and expected \"$EXPECTED_RESULT\"."
  exit 1
}