#!/bin/bash
set -o errexit
set -o pipefail

LABEL_SELECTOR="$1"
NAMESPACE="$2"

# If a namespace was specified, only search that specific namespace,
# otherwise search all namespaces.
NS_FILTER=
if [ -n "$NAMESPACE" ]; then
    NS_FILTER="-n ${NAMESPACE}"
else
    NS_FILTER="-A"
fi

# OCCM >= v1.23 will add the Openstack loadbalancer ID as an annotation on the service.
# We have confirmation from the Openstack Cloud team that the loadbalancer hostname will be
# generated as `lbaas-${LOADBALANCER_ID}.cern.ch`.
# Note: $LABEL_SELECTOR may be empty.
LB_HOSTNAMES=""
LB_IDS=$(oc get services ${NS_FILTER} -l "${LABEL_SELECTOR}" -o 'jsonpath={.items[*].metadata.annotations.loadbalancer\.openstack\.org/load-balancer-id}')
for LB_ID in $LB_IDS; do
  if [ -z "$LB_ID" ]; then
    # safe guard against empty elements
    continue
  fi
  LB_HOSTNAMES="${LB_HOSTNAMES}lbaas-${LB_ID},"
done
# remove trailing comma and print without newline
echo -n "${LB_HOSTNAMES%,}"