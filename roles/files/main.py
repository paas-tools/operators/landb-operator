#!/usr/bin/env python3

# Python script to serve as wrapper of the lanDB SOAP calls
# Some commands perform some extra actions that lanDB calls do not,
# for instance, setRemove will remove all the addresses inside the set
# before deleting it
from zeep import Client, Settings, exceptions, helpers # pip install zeep
import argparse
import json
import re
import pprint

LANDB_SOAP_ENDPOINT = "https://network.cern.ch/sc/soap/soap.fcgi?v=6&WSDL"

client = None
auth_header = None
args = None

# Python program to illustrate the intersection
# of two lists
def intersection(lst1, lst2):
	# Use of hybrid method
	temp = set(lst2)
	lst3 = [value for value in lst1 if value in temp]
	return lst3

# Initialize soap client and authentication header
def initializeClient(user, password):
    global client, auth_header

    accType="CERN"

    settings = Settings(strict=False)
    client = Client(LANDB_SOAP_ENDPOINT, settings=settings)
    token = client.service.getAuthToken(user, password, accType)
    auth_header = {'Auth': {'token': token}}

# -------------- BEGINNING OF LANDB DNS DELGATED DOMAIN RELATED FUNCTIONS --------------

# Add a new dns delegated entry
def dnsDelegatedAdd(domain, view):
    dns_delegated_input_type = client.get_type('ns0:DNSDelegatedInput')
    dns_delegated_input = dns_delegated_input_type(Domain=domain, View=view, KeyName=args.keyname, Description=args.ownership_string, UserDescription=args.description)
    try:
        client.service.dnsDelegatedAdd(dns_delegated_input, _soapheaders=auth_header)
    # We ignore the error of already exists to make this function reentrant
    except exceptions.Fault as ex:
        if not ex.message.startswith("DELEG_EXISTS"):
            raise ex

# Removes an existing dns delegated entry even if it contains some aliases
def dnsDelegatedRemove(domain, view):
    try:
        client.service.dnsDelegatedRemove(domain, view, _soapheaders=auth_header)
    # We ignore the error of not found to make this function reentrant
    except exceptions.Fault as ex:
        if not ex.message.startswith("NOTFOUND"):
            raise ex

# Searches all Delegated Domains that have a filed that matches search (domain, view, description, keyname)
def dnsDelegatedSearch(search_string):
    return client.service.dnsDelegatedSearch(search_string, _soapheaders=auth_header)

# Same exact call as the previous one but now we can search with a an extra parameter
def dnsDelegatedGetByNameView(search, extra):
    try:
        return client.service.dnsDelegatedGetByNameView(search, extra, _soapheaders=auth_header)
    except exceptions.Fault as ex:
        if ex.message.startswith("NOTFOUND"):
            return None
        else:
            raise ex

# This function searches for delegaed domains that match the string domain & view and then
# it asserts that Description of that domain matches the ownership_string
def dnsCheckOwnership(domain, view, ownership_string):
    if ownership_string == "":
        raise exceptions.Fault("MISSINGARG: No ownership string was provided, please provide one with --ownership-string")
    # Here we are adding a . before the name to match exactly how they are stored in the lanDB database
    # if we don't do this the search will not return any results
    list_of_dns = dnsDelegatedGetByNameView( "." + domain, view)
    if list_of_dns is None:
        return True
    current_ownership_string = list_of_dns["Description"]
    if current_ownership_string.lower() != ownership_string.lower():
        raise exceptions.Fault("INVALIDOWNER: We are trying to modify a delegate domain that belongs to another DelegatedDomain CR " + current_ownership_string )
    return True

# Checks if an Alias is present in a given DelegatedDomain entry
def isDelegatedAliasPresent(domain, view, alias):
    domain = f".{domain}"
    try:
        resp = dnsDelegatedSearch(domain)
        resp_obj = helpers.serialize_object(resp, dict)
        # print(resp_obj)
        aliases = []
        for a in resp_obj:
            if a.get('Domain').lower() == domain and a.get('View').lower() == view.lower():
                aliases = a.get('Aliases')
    except:
        aliases = []

    # print("Aliases", aliases)

    # compare case-insensitive
    if alias.lower() in [a.lower() for a in aliases]:
        return True

    return False

# Adds an alias to an existing dns entry if not already present
def dnsDelegatedAliasAdd(domain, view, alias):
    if isDelegatedAliasPresent(domain, view, alias):
      print(f"Alias '{alias}' already present for Domain '{domain}' in View '{view}', skipping insertion")
      return

    try:
        client.service.dnsDelegatedAliasAdd(domain, view, alias, _soapheaders=auth_header)
    except exceptions.Fault as ex:
        raise ex

    print(f"Alias '{alias}' added to existing DNS delegated domain '{domain}' in view '{view}' with success")


# Removes an alias to an existing dns entry if present
def dnsDelegatedAliasRemove(domain, view, alias):
    if not isDelegatedAliasPresent(domain, view, alias):
      print(f"Alias '{alias}' not present for Domain '{domain}' in View '{view}', skipping removal")
      return

    try:
        client.service.dnsDelegatedAliasRemove(domain, view, alias, _soapheaders=auth_header)
    except exceptions.Fault as ex:
        raise ex

    print(f"Alias '{alias}' removed from existing DNS delegated domain '{domain}' in view '{view}' with success")

# -------------- END OF LANDB DNS DELGATED DOMAIN RELATED FUNCTIONS --------------


# -------------- BEGINNING OF LANDB SET RELATED FUNCTIONS --------------

# Try to add a new set if set already exists ignore
def setInsert(landb_set_name):
    person_type = client.get_type('ns0:Person')
    person = person_type(Name=args.person_name, FirstName=args.person_first_name)

    set_info_type = client.get_type('ns0:SetInfo')
    set_info = set_info_type(ID=1, Name=landb_set_name, ResponsiblePerson=person, Description=args.ownership_string, Type=args.type, Domain=args.domain)

    try:
        client.service.setInsert(set_info, _soapheaders=auth_header)
    # We ignore the error of already exists to make this function reentrant
    except exceptions.Fault as ex:
        if not ex.message.startswith("SETEXISTS"):
            raise ex

# Removes the nodes from a set and then it removes the empty set from landb
def setRemove(landb_set_name):
    # Landb only provides a method to remove empty sets, so first we have to remove
    # all nodes from the set
    landb_nodes = getSetAllAddresses(landb_set_name)
    if landb_nodes == None:
        print("There are no interfaces in this set. Proceeding with set removal")
    else:
        # Remove nodes from set
        for node in landb_nodes:
            setDeleteAddress(landb_set_name, node)

    try:
        client.service.setRemove(landb_set_name, _soapheaders=auth_header)
    # We ignore the error of not found to make this function reentrant
    except exceptions.Fault as ex:
        if ex.message.startswith("NOTFOUND"):
            print("Set not found")
        else:
            raise ex

# Returns all landb sets that match the given pattern
def searchSet(pattern):
    return client.service.searchSet(pattern, _soapheaders=auth_header)

# Return info about the set that matches the given name
def getSetInfo(landb_set_name):
    try:
        return client.service.getSetInfo(landb_set_name, _soapheaders=auth_header)
    except exceptions.Fault as ex:
        if ex.message.startswith("NOTFOUND"):
            return None
        else:
            raise ex

# This function searches for sets that match the set name and then
# it asserts that Description of that set matches the ownership_string
def setCheckOwnership(landb_set_name, ownership_string):
    if ownership_string == "":
        raise exceptions.Fault("MISSINGARG: No ownership string was provided, please provide one with --ownership-string")
    existing_set = getSetInfo(landb_set_name)
    if existing_set is None:
        return True
    current_ownership_string = existing_set["Description"]
    if current_ownership_string.lower() != ownership_string.lower():
        raise exceptions.Fault("INVALIDOWNER: We are trying to modify a LanDB Set that belongs to another LandbSet CR " + current_ownership_string )
    return True

# Get all addresses in a set
def getSetAllAddresses(landb_set_name):
    try:
        return client.service.getSetAllInterfaces(landb_set_name, _soapheaders=auth_header)
    except exceptions.Fault as ex:
        if ex.message.startswith("NOTFOUND"):
            return None
        else:
            raise ex

# Add an address to a set
def setInsertAddress(landb_set_name, machineName):
    client.service.setInsertAddress(landb_set_name, machineName, _soapheaders=auth_header)

# Remove an address from a set
def setDeleteAddress(landb_set_name, machineName):
    client.service.setDeleteAddress(landb_set_name, machineName, _soapheaders=auth_header)

# -------------- END OF LANDB SET RELATED FUNCTIONS --------------

def main():
    global args

    # Parsing of arguments
    parser = argparse.ArgumentParser(description="Going to parse arguments")

    # Parameters common to all commands
    parser.add_argument('--command', required=True, choices=['set-add', 'set-remove', 'set-update', 'set-add-address', 'set-remove-address', 'dns-delegate-add', 'dns-delegate-remove', 'dns-delegate-remove-if-owned', 'dns-search', 'dns-delegate-alias-add', 'dns-delegate-alias-remove', 'set-search', 'set-get-info'],\
         help='set-* are commands that affect lanDB sets, dns-delegate-* are commands that affect lanDB DNS degated domains')
    parser.add_argument('--landb-user', dest='user', required=True, type=str, help='username to be used to preform \
    the landb SOAP calls')
    parser.add_argument('--landb-password', dest='password', required=True, type=str, help='password to be used to \
    preform the landb SOAP calls')
    parser.add_argument('--unprotected-command', dest='unprotected_command', required=False, default=False, action='store_true', help='Skip \
    check ownership check for both Sets and Delegated domains, default is False')
    parser.add_argument('--ownership-string', dest='ownership_string', type=str, default="", help='string used to identify \
    owner of resources, this will live in the description field of the resources. State modifying command require it')

    # Common but optional for some commands
    # Required for set-add and dns-delegate-*
    parser.add_argument('--domain', type=str, default="", help='if command is set-* then domain is domain of the \
        landb set; if command is dns-delegate-* then domain is the domain of the DNS delegated domain')

    # Parameters common to all set-* commands
    parser.add_argument('--landb-set-name', dest='landb_set_name', default="", type=str, help='name of the new \
        lanDB set to be created')

    # Parameters specific to set-add
    parser.add_argument('--person-name', dest='person_name', default="", type=str, help='name of the person who\'s going \
        to be responsible by the landb set')
    parser.add_argument('--person-first-name', dest='person_first_name', default="", type=str, help='fisrtName of the person \
        who\'s going to be responsible by the landb set')
    parser.add_argument('--type', type=str, default="", help='type of the landb set')

    # Parameters specific to set-add-address and set-remove-address
    parser.add_argument('--address', type=str, default="", help='FQDN to be added to the set provided')

    # Parameters specfic to set-update
    parser.add_argument('--node-list', dest='node_list', type=str, default="", help='List of K8S nodes to be added to the set provided')

    # Parameters common to all dns-delegate-*
    parser.add_argument('--view', type=str, default="", help='view of the DNS delegated domain')

    # Parameters specific to dns-delegate-add
    parser.add_argument('--keyname', type=str, default="", help='name of the DNS key with privileges to add/remove \
        lanDB delegated domains')
    parser.add_argument('--description', type=str, default="", help='description of the dns delegated \
        domain it will be the userDescription since description is used for ownership of resources')

    # Parameters common to all dns-delegate-alias-*
    parser.add_argument('--alias', type=str, default="", help='alias to be added/removed to/from DNS delegated domain \
        lanDB delegated domains')

    args = parser.parse_args()

    # Initialize lanDB client
    initializeClient(args.user, args.password)

    # Logic to handle set commands, non disruptive commands never check owenership of resources;
    # disruptive commands have to check ownership of resources unless they are runned with
    # unprotected-command flag
    if args.command.startswith("set-"):
        if args.landb_set_name == "":
            print("When using set commands, you have to at least provide --landb-set-name")
            return 1

        elif args.command == 'set-search':
            list_of_sets = searchSet(args.landb_set_name)
            if list_of_sets == []:
                print("ERROR: No sets found that matched the provided name/expression")
                return 1

            print(json.dumps(helpers.serialize_object(list_of_sets, dict)))
            return 0

        elif args.command == 'set-get-info':
            set_info = getSetInfo(args.landb_set_name)
            if set_info == None:
                print("ERROR: No set found that matched the provided name")
                return 1
            else:
                print(json.dumps(helpers.serialize_object(set_info, dict)))

            return 0

        try:
            if args.unprotected_command or setCheckOwnership(args.landb_set_name, args.ownership_string):

                if args.command == 'set-add':
                    if  args.domain == "" or args.person_name == "" or args.type == "":
                        print("When using command set-add, you have to provide --landb-set-name, --domain, --person-name and --type")
                        return 1

                    setInsert(args.landb_set_name)

                    print("Set created with success")

                elif args.command == 'set-remove':
                    setRemove(args.landb_set_name)

                    print("Set removed with success")

                elif args.command == 'set-add-address':
                    if  args.address == "":
                        print("When using command set-add-address, you have to provide --landb-set-name and --address")
                        return 1

                    setInsertAddress(args.landb_set_name, args.address)

                    print("Address added to set with success")

                elif args.command == 'set-remove-address':
                    if args.address == "":
                        print("When using command set-remove-address, you have to provide --landb-set-name and --address")
                        return 1

                    setDeleteAddress(args.landb_set_name, args.address)

                    print("Address removed from set with success")

                elif args.command == 'set-update':
                    if args.landb_set_name == "" or args.node_list == "":
                        print("When using command set-remove-address, you have to provide --landb-set-name and --node-list")
                        return 1

                    # Fetch nodes in set
                    landb_nodes = getSetAllAddresses(args.landb_set_name)
                    if landb_nodes == None:
                        print("Set not found on create so setInsert failed silenty")
                        return 1

                    #Fetch Openshift machines and add their names to a list named okd_nodes
                    parsed_names = args.node_list.split(",")
                    okd_nodes = []
                    for name in parsed_names:
                        if name != "":
                            okd_nodes.append((name+".cern.ch").upper())

                    # Find the intersection of landb_nodes and okd_nodes which gives us
                    # nodes that we don't want to touch
                    common_nodes = intersection(landb_nodes, okd_nodes)

                    # Add missing nodes
                    for node in okd_nodes:
                        if node not in common_nodes:
                            setInsertAddress(args.landb_set_name, node)

                    # Remove decommissioned nodes
                    for node in landb_nodes:
                        if node not in common_nodes:
                            setDeleteAddress(args.landb_set_name, node)


                    print("Set updated with success")

                    return 0

        except exceptions.Fault as ex:
            # Ignore ownership exception on remove operations so we don't endup not being able to
            # remove the finalizer from the CR, due to the script being always raising exceptions
            if "remove" in args.command and ex.message.startswith("INVALIDOWNER"):
                print("Nothing to do: LanDB set is not owned by this CR")
                return
            else:
                raise ex

    # Logic to handle dns- commands, non disruptive commands never check owenership of resources;
    # disruptive commands have to check ownership of resources unless they are runned with
    # unprotected-command flag, exception for commands with -alias- since, those are
    # going to be executed from another Ansible task that doesn't have access to ownership information
    elif args.command.startswith("dns-"):
        if args.domain == "":
            print("When using dns commands, you have to at least provide --domain")
            return 1

        elif args.command == 'dns-search':
            list_of_dns = dnsDelegatedSearch(args.domain)
            if not list_of_dns:
                print("ERROR: No delegated domains found that matched the provided name/expression")
                return 1
            else:
                print(json.dumps(helpers.serialize_object(list_of_dns, dict)))

            return 0

        elif args.view == "":
            print("When using dns commands to modify state, you have to at least provide --domain and --view")
            return 1

        elif args.command == 'dns-delegate-alias-add':
            if args.alias == "":
                print("When using command dns-delegate-alias-add, you have to provide --domain, --view, --alias")
                return 1
            else:
                dnsDelegatedAliasAdd(args.domain, args.view, args.alias)
            return 0

        elif args.command == 'dns-delegate-alias-remove':
            if args.alias == "":
                print("When using command dns-delegate-alias-remove, you have to provide --domain, --view, --alias")
                return 1
            else:
                dnsDelegatedAliasRemove(args.domain, args.view, args.alias)
            return 0

        try:
            if args.unprotected_command or dnsCheckOwnership(args.domain, args.view, args.ownership_string):

                if args.command == 'dns-delegate-add':
                    if args.keyname == "" or args.description == "" or args.ownership_string == "":
                        print("When using command dns-delegate-add, you have to provide --domain, --view, --keyname --owneship-string and --description. \
                            Ownerhsip string needs to be provided as it will live in the description field that cannot be empty")
                        return 1

                    # Due to lanDB user-description only having space for 56 charcters we have to trim
                    # our description here
                    args.description = args.description[:56]

                    dnsDelegatedAdd(args.domain, args.view)
                    print("DNS delegated domain added with success")

                elif args.command == 'dns-delegate-remove':

                    dnsDelegatedRemove(args.domain, args.view)

                    print("DNS delegated domain removed with success")

                return 0

        except exceptions.Fault as ex:
            # Ignore ownership exception on remove operations so we don't endup not being able to
            # remove the finalizer from the CR, due to the script being always raising exceptions
            if "remove" in args.command and ex.message.startswith("INVALIDOWNER"):
                print("Nothing to do: DNS delegated domain is not owned by this CR")
                return
            else:
                raise ex

    else:
      raise exceptions.Fault("COMMANDNOTFOUND: command you are trying to run was not found")


if __name__ == "__main__":
    main()
