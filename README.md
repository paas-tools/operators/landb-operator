# LanDB Operator

This is an Ansible operator that consumes information from three CRDs
(`DelegatedDomain`, `DelegatedDomainAlias` and `LandbSet`) to then update LanDB through their SOAP calls.

Note that this Ansible Operator is based on [version 0.17.1](https://v0-17-x.sdk.operatorframework.io/docs/ansible/) ("pre-v1.0.0").

## How it works

Together with the Ansible roles there is also a Python script that implements all the logic of actually communicating with LanDB SOAP. Hence, once a CRD is created an Ansible role will be used to fetch the necessary information from the CRs and from Openshift to then call the Python script with the right parameters.

Usefulness of the different CRD:

- `DelegatedDomain`: Allows us to publish a delegated domain (the ["Dynamic Subdomain" of CERN DNS doc](https://service-dns.web.cern.ch/advanceddns.asp#lba));
- `DelegatedDomainAlias`: Allows us to publish an alias to a delegated domain (generates a `alias.cern.ch CNAME delegateddomain.cern.ch` record in CERN DNS);
-  `LandbSet`: Allows us to publish a LanDB set that will contain the all the cluster's public-facing IP addresses: nodes and services of type `LoadBalancer`.
   Optionally filter nodes with `labelSelector` and filter Loadbalancer services with `loadbalancerLabelSelector` and `loadbalancerNamespace`.
   If no label selector or namespace are specified then all resources will be added to the LanDB set.
   You can use a dummy label selector like `none=` if you don't want to include any nodes / loadbalancers in the set.

For more information on the capabilities of the script the use can run `python3 roles/files/main.py -h` note that not all [LanDB calls](https://network.cern.ch/sc/soap/6/description.html) are implemented

## Manual commands

The landb-operator is basically a wrapper to a python script that implements multiple commands to run against the LanDB SOAP API hence, if we have a valid and working deployment of the operator we are able to access the pod/container and run ourselves some manual commands in case we need to preform some emergency operations or simply clean up some unused resources from LanDB.

#### Accessing the pod/container
To acces the pod/container where the landb-operator we have to first get the name of the pod, for that we can run,
`oc get pods -n openshift-cern-landb`. After we have the pod name we can spawn a bash shell on it with the command:
```bash
oc exec -it deploy/landb-operator -n openshift-cern-landb -- /bin/bash
```

Now we are ready to directly run any command in the python script.

### LanDB Sets

#### Listing all the LanDB sets

LanDB only provides us with a SOAP call that allows us to search through all the LanDB sets names that match a given pattern. We can do this by
running the following command.

```bash
python3 ${HOME}/roles/files/main.py --landb-user ${LANDB_USER} --landb-password ${LANDB_PASSWORD} --command set-search --landb-set-name ${STRING_PATTERN}
```

Since all our OKD4 clusters start with the name "IT OKD" we can list them all with the command
```bash
python3 ${HOME}/roles/files/main.py --landb-user ${LANDB_USER} --landb-password ${LANDB_PASSWORD} --command set-search --landb-set-name "%it okd%" | python3 -m json.tool
```

#### Get more information about a LanDB Set

If we want to get more information about a set we can run:
```bash
python3 ${HOME}/roles/files/main.py --landb-user ${LANDB_USER} --landb-password ${LANDB_PASSWORD} --command set-get-info --landb-set-name ${SET_NAME}
```

#### Deleting a Set

If we want to remove a set from LanDB we can run:
```bash
python3 ${HOME}/roles/files/main.py --landb-user ${LANDB_USER} --landb-password ${LANDB_PASSWORD} --command set-remove --landb-set-name ${SET_NAME} --unprotected-command
```

This command will remove the Delegated Domain and all the Aliases inside it. The `--unprotected-command` flag is necessary as this command is protected by an owership string.

### Delegated Domains
#### Listing all Delegated Domains

LanDB provides us with a SOAP call that allows us to search for a dns delegated entry that matches a given a pattern:

```bash
python3 ${HOME}/roles/files/main.py --landb-user ${LANDB_USER} --landb-password ${LANDB_PASSWORD} --command dns-search --domain ${STRING_PATTERN} | python3 -m json.tool
```

The pattern is matched against all the atributes of a delegated domain (domain, view, description, keyname) so we can get a list of all the Delegated Domains our key owns by running:

```bash
python3 ${HOME}/roles/files/main.py --landb-user ${LANDB_USER} --landb-password ${LANDB_PASSWORD} --command dns-search --domain %ITWEBSERVICES%
```

This LanDB call will already give us all the information about the delegated domains we searched for.

#### Delete a delegated domain

If we want to remove a DelegatedDomain from LanDB we can run:
```bash
python3 ${HOME}/roles/files/main.py --landb-user ${LANDB_USER} --landb-password ${LANDB_PASSWORD} --command dns-delegate-remove --domain ${DELEGATED_DOMAIN} --view ${VIEW} --unprotected-command
```

Eg.
```bash
python3 ${HOME}/roles/files/main.py --landb-user ${LANDB_USER} --landb-password ${LANDB_PASSWORD} --command dns-delegate-remove --domain clu-estevesm.cern.ch --view internal --unprotected-command
```

The `--unprotected-command` flag is necessary as this command is protected by an owership string.

## Useful links

- [LanDB SOAP API description](https://network.cern.ch/sc/soap/6/description.html)
- [LanDB SOAP WSDL](https://network.cern.ch/sc/soap/soap.fcgi?v=6&WSDL)
- [Zeep doc](https://docs.python-zeep.org/en/master/client.html) (Python SOAP lib)
- [LanDB Sets](https://landb.cern.ch/landb/portal/sets/displaySets)
- [Ansible Operator Quickstart](https://v0-17-x.sdk.operatorframework.io/docs/ansible/quickstart/)
- [Ansible Operator Development](https://v0-17-x.sdk.operatorframework.io/docs/ansible/development-tips/)
